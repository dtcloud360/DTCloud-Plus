
const path = require('path')

function resolve(dir) {
  return path.join(__dirname, dir)
}
module.exports = {
  lintOnSave: false,
  configureWebpack: {
    name: 'vue',
    devServer: {
      port:8080,
      proxy: {
        '/api_other': {
          target:'http://49.234.222.178:8888',
          ws:true,
          secure:false,
          changeOrigin: true, //是否开启跨域
          pathRewrite: {
              '^/api_other': '' //让路径以/api开头的字段为空
          }
        }
      }
    },
    resolve: {
      alias: {
        '@': resolve('src'),
      }
    },
  },
  pluginOptions: {
    "style-resources-loader": {
      preProcessor: "less",
      patterns: [
        // 存放less变量文件的路径
        path.resolve(__dirname, "./src/assets/style/base.less")
      ]
    }
  },
  css: {
    loaderOptions: {
      less: {
        lessOptions: {
          modifyVars: {
            'primary-color': '#ec6800'
          },
          javascriptEnabled: true,
        },
      }
    },
  },
}
