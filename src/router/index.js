import { createRouter, createWebHashHistory } from 'vue-router'
import Layout from '../layout/index'

const routes = [
  {
    path: '/login',
    component: () => import('@/views/login'),
    meta: {
      title: '登录',
      isMenu: false,
    }
  },
  {
    path:'/',
    component: Layout,
    redirect: '/home',
    meta: {
      title: '首页',
      isMenu: true,
      index: '0'
    },
    children: [
      {
        path: '/home',
        name: 'home',
        component: () => import('@/views/home/index'),
        meta: { 
          title: '首页', menu: true, index: '1'
        }
      },
      {
        path:'/menu',
        name: 'menu',
        meta: { 
          title: '菜单',  isMenu: true, index: '2'
        },
        children: [
          {
            path: '/menuTwo',
            name: 'menuTwo',
            component: () => import('@/views/home/index'),
            meta: { 
              title: '菜单一', isMenu: true, index: '2-1'
            }
          },
          {
            path: '/menuThree',
            name: 'menuThree',
            component: () => import('@/views/home/index'),
            meta: { 
              title: '菜单二', isMenu: true, index: '2-2'
            },
            children: [
              {
                path: '/menuFour',
                name: 'menuFour',
                component: () => import('@/views/home/index'),
                meta: { 
                  title: '菜单二一', isMenu: true, index: '2-2-1'
                }
              },
              
              {
                path: '/menuFive',
                name: 'menuFive',
                component: () => import('@/views/home/index'),
                meta: { 
                  title: '菜单二二', isMenu: true, index: '2-2-2'
                }
              }
            ]
          }
        ]
      }
    ]
  },
]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

export default router
