import { createStore } from 'vuex'
import { merge } from 'lodash';
import layout from '@/store/module/layout';
import login from '@/store/module/login';

export default createStore({
  state: {},
  getters: {},
  mutations: {},
  actions: {},
  modules: {
    layout,
    login
  }
})
