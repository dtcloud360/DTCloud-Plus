import { merge } from 'lodash';
import { setLocalStorage, getLocalStroage } from '@/util/index'
import { toRaw } from 'vue';

const layout = {
  namespaced: true,
  state(){
    return {
      isExpandMenu: false, // 是否折叠menu菜单
      isRefresh: true, // 是否刷新
      defaultOpeneds: ['1','1-1','16','16-1'],
      styleOptions: {
        layoutType: getLocalStroage('styleOptions') && getLocalStroage('styleOptions').layoutType ? getLocalStroage('styleOptions').layoutType : 'vertical', // 布局
        theme: getLocalStroage('styleOptions') && getLocalStroage('styleOptions').theme ? getLocalStroage('styleOptions').theme : 'defaultTheme', // 主题
        showTabs: getLocalStroage('styleOptions') ? getLocalStroage('styleOptions').showTabs : true, // 是否展示标签
        showTabsIcon: getLocalStroage('styleOptions') ? getLocalStroage('styleOptions').showTabsIcon : true, // 是否展示标签图标
        isFixedHead: getLocalStroage('styleOptions') ? getLocalStroage('styleOptions').isFixedHead : true, // 是否固定头部
        showInternationalIcon: getLocalStroage('styleOptions') ? getLocalStroage('styleOptions').showInternationalIcon : true, // 是否展示国际化图标
        showRefreshIcon: getLocalStroage('styleOptions') ? getLocalStroage('styleOptions').showRefreshIcon : true, // 是否展示刷新图标
        showScreen: getLocalStroage('styleOptions') ? getLocalStroage('styleOptions').showScreen : true, // 是否展示全屏
      },
      tabList:[
        {
          path: '/home',
          name: 'home',
          meta: { title: '首页', menu: true, index: '1' }
        }
      ],// 菜单选择打开记录tab
      tabIndex: '1', // 记录tab下标
    }
  },
  mutations: {
    // 是否折叠menu菜单
    setExpandMenu(state){
      state.isExpandMenu = !state.isExpandMenu;
    },
    // 是否刷新
    setRefresh(state) {
      state.isRefresh = false;
      setTimeout(()=>{
        state.isRefresh = true;
      }, 500);
    },
    // 主题设置
    setStyleOptions(state,options) {
      merge(state.styleOptions, options);
      setLocalStorage('styleOptions', options);
    },
    // 主题恢复默认
    setDefaultStyleOptions(state) {
      state.styleOptions.showTabs = true;
      state.styleOptions.showTabsIcon = true;
      state.styleOptions.isFixedHead = true;
      state.styleOptions.showInternationalIcon = true;
      state.styleOptions.showRefreshIcon = true;
      state.styleOptions.showScreen = true;
      setLocalStorage('styleOptions', toRaw(state.styleOptions));
    },
    //
    setDefaultOpeneds(state, options){
      merge(state.defaultOpeneds, options);
    },
    // 记录点击的菜单项
    setTabList(state,options){
      let toggle = false; // 设置开关
      if(state.tabList.length == 0){
        toggle = true; // 第一次必添加
      } else {
        for(let i in state.tabList) {
          // 如果存在相同的tab，则toggle:false , 反之为true
          if(state.tabList[i].meta.index == options.meta.index){
            toggle = false
            break;
          } else {
            toggle = true;
          }
        }
      }
      if(toggle) { state.tabList.push(options) }
    },
    // 记录tab下标
    setTabIndex(state,options){
      state.tabIndex = options.meta.index;
    },
    // 删除tab
    removeTab(state,options){
      state.tabList.forEach((item,index) => {
        if(item.meta.index == options) {
          state.tabList.splice(index,1);
        }
      })
    },
    //关闭所有tabs
    closeAllTabs(state){
      state.tabIndex = '1';
      state.tabList.splice(1,state.tabList.length-1) 
    },
    //关闭左侧tabs
    closeLeftTabs(state){
      state.tabList.forEach((item,index) => {
        if(item.meta.index == state.tabIndex){
          state.tabList.splice(1,index - 1)
        }
      });
    },
    //关闭右侧tabs
    closeRightTabs(state){
      state.tabList.forEach((item,index) => {
        if(item.meta.index == state.tabIndex){
          state.tabList.splice(index + 1, state.tabList.length -1)
        }
      });
    },
  },
  actions: {
    // 关闭其他tabs
    closeOtherTabs(context){
      context.commit('closeLeftTabs')
      context.commit('closeRightTabs')
    }
  },
}
export default layout;