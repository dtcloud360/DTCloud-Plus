import { merge } from 'lodash';
import { setLocalStorage, getLocalStroage } from '@/util/index';

const login = {
  namespaced: true,
  state(){
    return {
      token: getLocalStroage('token') || ''
    }
  },
  getters: {},
  mutations: {},
  actions: {}
};

export default login;