import axios from 'axios';
import store from '@/store/index'

const baseURL = process.env.NODE_ENV == 'production' ? '' : '/api_other';
console.log(store)

const token = store.state.login.token;

const service = axios.create({
  baseURL,
  timeout: 10000,
  headers: {
    'Content-Type': 'application/x-www-form-urlencoded',
    'Access-Control-Allow-Origin': '*'
  }
});

// 请求拦截
service.interceptors.request.use((config) => {
    console.log('request:',config)
    // const isToken = (config.headers || {}).isToken == false;
    // if(token && !isToken) {
    //   config.headers['Authorization'] = 'Bearer' + token
    // }
    return config;
  },
  (error) => {
    return Promise.reject(error);
  }
)
// 响应拦截
service.interceptors.response.use((response) => {
  console.log('response',response)
  return response;
},(error) => {
  return Promise.reject(error)
})

export default service