import request from '../util/request';

export const login = (data) => {
  return request({
    url: '/api/v1/login',
    data,
    method: 'post'
  })
}