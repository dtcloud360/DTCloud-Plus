const path = require('path');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
module.exports = {
  module: {
    rules: [
      { 
        test: /\.txt$/, 
        use: 'raw-loader' 
      },
      {
        test: /\.(t|j)s$/,
        use: {
          loader: "babel-loader",
          options: {
            presets: ['@babel/preset-env',]
          }
        },
        exclude: /node_modules/
      },
      {
        test: /\.(vue)$/,
        use: {
          loader: 'vue-loader'
        }
      },
      // {
      //   test: /.(png|jpe?g|gif|svg)(\?.*)?$/,
      //   loader: 'url-loader',
      //   options: {
      //     outputPath: 'static/img',
      //     publicPath: 'static/img',
      //     name: '[name].[ext]'
      //   }
      // },
      {
        test: /\.(png|svg|jpg|gif)$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              limit: 5000,
              name: 'imgs/[hash].[ext]'
            }
          }
        ]
      }, 
      {
        test: /\.less$/,
        use: [
          'style-loader',
          { loader: 'css-loader' },
          { loader: 'less-loader' },
          {
            loader: "style-resources-loader",
            options: {
              patterns: [
                path.resolve(__dirname, "../src/assets/style/base.less"),
              ],
            },
          },
        ]
      },
      {
        test: /\.css$/,
        use: [
            MiniCssExtractPlugin.loader,
            "css-loader",
        ],
      },
      {
        test: /\.sass$/,
        use: [ 
          // { loader: 'style-loader' },
          { loader: 'css-loader' },
          // { loader: 'less-loader' },
          { loader: 'sass-loader' }
        ]
      }
    ]
  },
}