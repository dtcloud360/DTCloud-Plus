const { merge } = require("webpack-merge");
const WebpackBaseConfig = require("./webpack.base.config");
const WebpackRulesConfig = require("./webpack.rules.config");

module.exports = merge(WebpackBaseConfig,{
  mode: "development",
  devServer: {
    port: 8080,
    hot: true
  },
  module: WebpackRulesConfig.module,
});