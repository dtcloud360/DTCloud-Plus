const path = require('path');
const { merge } = require("webpack-merge");
const WebpackBaseConfig = require("./webpack.base.config");
const WebpackRulesConfig = require("./webpack.rules.config");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const TerserPlugin = require("terser-webpack-plugin");
const copyWebpackPlugin = require("copy-webpack-plugin");

module.exports = merge(WebpackBaseConfig,{
  mode: "production",
  module: WebpackRulesConfig.module,
  plugins: [
    new copyWebpackPlugin({
      patterns: [
        {
          from: path.resolve(__dirname, "../public"),
          to: "./",
          globOptions: {
            dot: true,
            gitignore: true,
            ignore: ["**/index.html*"],
          },
        },
      ],
    }),
    new MiniCssExtractPlugin({
      filename: "./css/[name].[contenthash].css",
      chunkFilename: "./css/[id].[contenthash].css",
    }),
  ],
  optimization: {
    minimize: true,
    minimizer: [
      new TerserPlugin({
        parallel: true,
        extractComments: false,
        terserOptions: {
          compress: {
            drop_console: true,
            drop_debugger: true,
          },
        },
      }),
    ],
  },
});