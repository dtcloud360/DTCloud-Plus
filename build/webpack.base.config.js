const path = require('path');
const { VueLoaderPlugin } = require('vue-loader');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { extractIdentifiers } = require('@vue/compiler-sfc');

module.exports = {
  entry: {
    path: path.resolve(__dirname, "../src/main.js"),
    // vendor: ["axios", "vue-router", "vuex"],
  },
  output: {
    path: path.resolve(__dirname, '../dist'),
    filename: 'js/[name].bundle.js'
  },
  plugins: [
    new VueLoaderPlugin(),
    new HtmlWebpackPlugin({
      title: '平台',
      template: path.join(__dirname,'../public/index.html'),
      favicon: path.join(__dirname,'../public/favicon.ico'),
      filename: 'index.html',
      inject: 'body',
      minify: {
        html5: true,
        removeComments: false, // 移除注释
        preserveLineBreaks: false,
        minifyCSS: true, // 压缩css
        minifyJS: true, // 压缩js
        collapseWhitespace: true,//去除空格
      }
    })
  ],
  resolve: {
    extensions: [".ts", ".js", ".vue", ".json"],
    mainFields: ["jsnext:main", "module", "browser", "main"],
    alias: {
      '@': path.resolve(__dirname,'../src'),
    },
  }
};